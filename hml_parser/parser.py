"""The juicy stuff, the HML parser."""
import pandas
from dateutil.parser import parse as dateutil_parse


def log_iterator(file_path):
    """Iterate over every log entry in the file."""
    file = open(file_path)
    try:

        rows = []
        first_log_line_seen = False
        for line in file:

            first_log_line_detected = line.startswith('00')
            if first_log_line_detected and not first_log_line_seen:
                first_log_line_seen = True
            elif first_log_line_detected and first_log_line_seen:
                yield rows
                rows = []

            columns = [column.strip() for column in line.split(',')]
            rows.append(columns)

        yield rows

    finally:
        file.close()


def get_log_title(log_rows):
    """Return a formated title from the log rows."""
    title_row = [row for row in log_rows if row[0] == '00']
    title_columns = title_row and title_row[0]

    sub_title_row = [row for row in log_rows if row[0] == '01']
    sub_title_columns = sub_title_row and sub_title_row[0]

    columns = ', '.join(get_data_columns(log_rows)).strip()

    title = '{} - {}: {} --> {}'.format(
        title_columns[1], title_columns[2], sub_title_columns[2], columns,
    )
    return title


def get_data_frame(log_rows):
    """Return a panda data frame from the log rows."""
    data = []
    index = []
    columns = []

    for row in log_rows:
        if row[0] == '80':
            data_columns = row[2:]
            data.append([float(column) for column in data_columns])

            timestamp = row[1]
            index.append(dateutil_parse(timestamp))

    columns = get_data_columns(log_rows)

    return pandas.DataFrame(data=data, index=index, columns=columns)


def get_data_columns(log_rows):
    """Return columns names with it's data type from the log rows."""
    data_columns = []

    column_labels = []
    column_types = []
    for row in log_rows:
        if row[0] == '02':
            column_labels = row[2:]
        elif row[0] == '03':
            column_types.append(row[3])

    for column_label, column_type in zip(column_labels, column_types):
        data_columns.append('{} ({})'.format(column_label, column_type))

    return data_columns
