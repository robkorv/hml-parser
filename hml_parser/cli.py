"""Command line interpreter."""
import click

from . import parser


@click.command()
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=True,
        dir_okay=False,
        writable=False,
        readable=True,
        resolve_path=False,
        allow_dash=False,
        path_type=None,
    ),
)
@click.option('-l', '--log-entry', type=click.INT)
def main(path, log_entry):
    """
    CLI for parsing hml files.

    - path: path to *.hml file you want to parse.
    """
    for i, log in enumerate(parser.log_iterator(path), start=1):
        if log_entry and log_entry == i or not log_entry:
            click.echo('{} | {}'.format(i, parser.get_log_title(log)))
        if log_entry and log_entry == i:
            data_frame = parser.get_data_frame(log)
            click.echo(data_frame.quantile([0.25, 0.50, 0.75, 1.0]))
            click.echo(data_frame.describe(percentiles=[0.01, 0.95, 0.99]))

    if not log_entry:
        click.echo('use -l # or --log-entry # to parse one of the logs.')


if __name__ == '__main__':  # pragma: no cover
    main()  # pylint: disable=no-value-for-parameter
