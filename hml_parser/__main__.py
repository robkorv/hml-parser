"""Cli start for when package is executed with `python -m`."""
if __name__ == '__main__':  # pragma: no cover
    from .cli import main
    main()  # pylint: disable=no-value-for-parameter
