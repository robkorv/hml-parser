"""Test cli."""
from click.testing import CliRunner

from hml_parser import cli


def test_cli_main():
    """Test cli.main."""
    path = 'tests/files/HardwareMonitoring.hml'
    runner = CliRunner()

    result = runner.invoke(cli.main, [path])
    assert result.exit_code == 0
    assert (
        '2 | 18-12-2018 22:02:11 - Hardware monitoring log v1.5: GeForce GTX 1080 --> '
        'Framerate (FPS), Frametime (ms)'
    ) in result.output
    assert (
        '0.25'
    ) not in result.output

    result = runner.invoke(cli.main, [path, '-l', 1])
    assert result.exit_code == 0
    assert (
        '2 | 18-12-2018 22:02:11 - Hardware monitoring log v1.5: GeForce GTX 1080'
    ) not in result.output
    assert (
        '1 | 18-12-2018 21:50:05 - Hardware monitoring log v1.5: GeForce GTX 1080 --> '
        'GPU usage (%), Memory usage (MB), CPU usage (%), RAM usage (MB), Framerate (FPS), '
        'Frametime (ms)'
    ) in result.output
    assert (
        '0.25'
    ) in result.output
