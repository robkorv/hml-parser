"""Setup for setuptools."""
from setuptools import setup

setup(
    name='hml-parser',
    version='0.1',
    packages=['hml_parser'],
    install_requires=[
        'bottleneck',
        'click',
        'numexpr',
        'pandas',
        'python-dateutil',
    ],
    extras_require={
        'dev': [
            'flake8',
            'flake8-commas',
            'flake8-docstrings',
            'isort',
            'pep8-naming',
            'pylint',
            'pytest',
            'pytest-cov',
        ],
    },
    entry_points={
        'console_scripts': [
            'hml_parser=hml_parser.cli:main',
        ],
    },
)
